Feature: Authentication

  In order to create and manage locations
  I should be able to authenticate

  Scenario: Sign up
    When I sign up
    Then I should have an account
    And I should be logged in

  Scenario: Log in
    Given I am a registered user
    When I log in 
    Then I should be logged in 

  Scenario: Log out
    Given I am a registered user 
    And I log in 
    When I log out 
    Then I should not be logged in 