Feature: Locations

  In order to visit the city
  I should be able to manage locations 

  Scenario: Listing locations
    Given There are locations in the system
    When I visit the locations page
    Then I should see a list of the locations

  Scenario: Reading a location
    Given There are locations in the system 
    When I select a location 
    Then I should see the location's details

  Scenario: Creating a location
    Given I am a registered user
    And I log in 
    When I create a new location 
    Then I should see the new location in the list 

  Scenario: Changing a location's name
    Given I am a registered user 
    And I log in 
    And I have created a location 
    When I change the location's name 
    Then I should see the new name in the list 
    And I should not see the old name in the list

  Scenario: Deleting a location
    Given I am a registered user 
    And I log in 
    And I have created a location 
    When I delete the location 
    Then I should not see the location's name in the list 

  Scenario: Editing a location from another user
    Given I am a registered admin
    And I log in
    And There is a location from a different user 
    When I change the location's name 
    Then I should see the new name in the list 
    And I should not see the old name in the list

  Scenario: Deleting a location from another user
    Given I am a registered admin
    And I log in
    And There is a location from a different user 
    When I delete the location 
    Then I should not see the location's name in the list 
