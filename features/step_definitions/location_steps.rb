Given("There are locations in the system") do
  FactoryBot.create_list(:location, 3)
end

When("I visit the locations page") do
  visit locations_path
end

Then("I should see a list of the locations") do
  Location.all.each do |location|
    expect(page).to have_content(location.name)
  end
end

When("I select a location") do
  @location = Location.first
  visit locations_path
  click_on @location.name
end

Then("I should see the location's details") do
  expect(page).to have_content @location.name
  expect(page).to have_content @location.description
  expect(page).to have_content @location.category
end

When("I create a new location") do
  @location = FactoryBot.build(:location, user: nil)
  visit new_location_path
  fill_in "location_name", with: @location.name
  fill_in "location_description", with: @location.description
  fill_in "location_latitude", with: @location.latitude
  fill_in "location_longitude", with: @location.longitude
  fill_in "location_image", with: @location.image
  select @location.category, from: "location_category"
  click_on "Save"
end

Then("I should see the new location in the list") do
  visit locations_path 
  expect(page).to have_content(@location.name)
end

Given("I have created a location") do
  @location = FactoryBot.create(:location, user: @user)
end

When("I change the location's name") do
  visit location_path(@location)
  click_on "Edit"
  @new_name = Faker::Games::Zelda.location
  @old_name = @location.name 
  fill_in "location_name", with: @new_name
  click_on "Save"
end

Then("I should see the new name in the list") do
  visit locations_path
  expect(page).to have_content(@new_name)
end

Then("I should not see the old name in the list") do
  visit locations_path
  expect(page).not_to have_content(@old_name)
end

When("I delete the location") do
  visit location_path(@location)
  click_on "Delete"
end

Then("I should not see the location's name in the list") do
  visit locations_path
  expect(page).not_to have_content(@location.name)
end

Given("I am a registered admin") do
  @user = FactoryBot.create(:user, admin: true)
end

Given("There is a location from a different user") do
  @location = FactoryBot.create(:location)
end
