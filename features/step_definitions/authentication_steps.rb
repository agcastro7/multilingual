When("I sign up") do
  @user = FactoryBot.build(:user)
  visit sign_up_path
  fill_in "user_name", with: @user.name 
  fill_in "user_email", with: @user.email
  fill_in "user_password", with: @user.password
  fill_in "user_password_confirmation", with: @user.password
  click_on "Sign up"
end

Then("I should have an account") do
  expect(User.last.email).to eq(@user.email)
end

Then("I should be logged in") do
  expect(page).to have_content("New location")
end

Given("I am a registered user") do
  @user = FactoryBot.create(:user)
end

When("I log in") do
  visit sign_in_path
  fill_in "user_email", with: @user.email 
  fill_in "user_password", with: @user.password 
  click_on "Log in"
end

Given("I am logged in") do
  pending # Write code here that turns the phrase above into concrete actions
end

When("I log out") do
  click_on "Sign out"
end

Then("I should not be logged in") do
  expect(page).not_to have_content("New location")
end
