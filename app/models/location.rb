class Location < ApplicationRecord
  belongs_to :user

  def editable_by_user?(u)
    u.admin? || user_id == u.id
  end
end
