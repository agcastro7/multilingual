module LocationsHelper
  def editable_by_current_user?(location)
    user_signed_in? && location.editable_by_user?(current_user)
  end

  def categories_for_select
    ["Monument", "Point of interest", "Attraction"]
  end
end
