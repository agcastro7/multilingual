admin = FactoryBot.create(:user, name: "Admin", email: "admin@admin.com", admin: true)
regular_user = FactoryBot.create(:user, name: "Regular user", email: "user@user.com", admin: false)

FactoryBot.create(:location, name: "The London Eye", image: "https://lh5.googleusercontent.com/p/AF1QipOzqJ8fovLfja5O90uaXQdPEVF3MtQM_0pn6e-6=w426-h240-k-no", latitude: 51.5033273, longitude: -0.121737, user: admin, category: "Attraction")

FactoryBot.create(:location, name: "Buckingham Palace", image: "https://lh5.googleusercontent.com/p/AF1QipN16bj0kRAsrcb30MWuwWMlWgbRBqf39Nti_w8k=w408-h322-k-no", latitude: 51.5013673, longitude: -0.144084, user: admin)

FactoryBot.create(:location, name: "Piccadilly Circus", image: "https://geo1.ggpht.com/maps/photothumb/fd/v1?bpb=CiwKKnNlYXJjaC5nd3MtcHJvZC9tYXBzL2xvY2FsLWRldGFpbHMtZ2V0Y2FyZBIgChIJwR8g_9MEdkgR_rI--wzfivAqCg0AAAAAFQAAAAAaBgjwARCYAw&gl=ES", latitude: 51.5099728, longitude: -0.1371652, user: regular_user, category: "Point of interest")

FactoryBot.create(:location, name: "Whitechapel", image: "https://lh5.googleusercontent.com/p/AF1QipO4_uEGu7EQ4UjO7w6T4b1xoQahpgEUJLRBLuGx=w408-h725-k-no", latitude: 51.5142551, longitude: -0.0720221, user: regular_user, category: "Point of interest")