# README

To download and use this app:

```bash 
git clone git@bitbucket.org:agcastro7/multilingual.git
cd multilingual
bundle
yarn install
rails db:migrate
rails db:seed
rails s
```

To run tests (after install):
```
cucumber
```