Rails.application.routes.draw do
  resources :locations
  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }

  as :user do
    get 'sign-up', to: 'devise/registrations#new', as: :sign_up
    get 'sign-in', to: 'devise/sessions#new', as: :sign_in
    delete 'sign-out', to: 'devise/sessions#destroy', as: :sign_out
  end
  root 'locations#index'
end
