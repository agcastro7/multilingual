require 'rails_helper'

RSpec.describe "locations/index", type: :view do
  before(:each) do
    assign(:locations, [
      Location.create!(
        :name => "Name",
        :description => "MyText",
        :latitude => 2.5,
        :longitude => 3.5,
        :type => "Type",
        :user => nil
      ),
      Location.create!(
        :name => "Name",
        :description => "MyText",
        :latitude => 2.5,
        :longitude => 3.5,
        :type => "Type",
        :user => nil
      )
    ])
  end

  it "renders a list of locations" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 2.5.to_s, :count => 2
    assert_select "tr>td", :text => 3.5.to_s, :count => 2
    assert_select "tr>td", :text => "Type".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
