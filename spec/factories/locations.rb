FactoryBot.define do
  factory :location do
    name        { Faker::Games::Zelda.location }
    description { Faker::Lorem.sentence(word_count: 200) }
    latitude    { Faker::Address.latitude }
    longitude   { Faker::Address.longitude }
    category    { "Monument" }
    image       { Faker::LoremPixel.image(size: "300x300") }
    user
  end
end
